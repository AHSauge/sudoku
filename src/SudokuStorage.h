/*
 * SudokuStorage.h
 *
 *  Created on: 6 May 2019
 *      Author: Alf Henrik Sauge
 */

#ifndef SUDOKUSTORAGE_H_
#define SUDOKUSTORAGE_H_

#include <stack>
#include <stdint.h>
#include <vector>

namespace Sudoku
{
    enum class ConflictLevel
    {
        None, SameDimention, Direct
    };

    enum class Placement
    {
        Row, Column, Block
    };

    enum class PencilMark
    {
        None, WasManuallyFilled, AutoFilled, ManuallyRemoved, ManuallyFilled,
    };

    struct Cell
    {
        bool Locked;
        uint8_t Number;
        uint8_t Block;
        std::vector<uint8_t> PencilMarks;
        bool ConflictSource;
        Cell(bool l, uint8_t n, uint16_t dim)
        : Locked(l),
          Number(n),
          Block(0),
          PencilMarks(std::vector<uint8_t>(0, dim)),
          ConflictSource(false)
        { }
    };

    struct Conflict
    {
        ConflictLevel Level;
        bool HighLight;
        Conflict()
        : Level(ConflictLevel::None),
          HighLight(false)
        { }
    };

    struct Coordinate
    {
        uint8_t Row;
        uint8_t Col;

        Coordinate(uint8_t r, uint8_t c)
        : Row(r),
          Col(c)
        { }
    };

    struct CellFilter
    {
        uint16_t ReferencedID;
        Cell* ReferencedCell;
    };

    struct State
    {
        std::vector<std::vector<bool>> Possibles;
        std::vector<std::vector<PencilMark>> PencilMarks;
    };

    struct Change
    {
    	State PreviousState;
    	Cell* ChangedCell;
    	uint8_t PreviousValue;
    };

    class Storage
    {
    public:
        Storage();
        virtual ~Storage();

        uint16_t getNumCells() const;
        uint8_t getDimention() const;
        uint8_t getBlockDimention() const;
        bool setNumber(uint16_t cellID, uint8_t num);
        uint8_t getNumber(uint16_t cellID);
        const Cell& getCell(uint16_t cellID) const;
        ConflictLevel getConflict(uint16_t cellID) const;
        const std::vector<PencilMark>& getPencilMarks(uint16_t cellID) const;
        bool togglePencilMark(uint16_t cellID, uint8_t number);

        void setDefaultBlockFilter();

        void undo();

        void setAutoPencil(bool autoOn);

    private:
        void updateConflict(Cell &source);
        void updatePossible();
        void applyConflict(uint16_t a, uint16_t b, Placement type);
        Coordinate getCoordinate(uint16_t cellID) const { return Coordinate(cellID / m_Dimention, cellID % m_Dimention); }

    private:
        std::stack<Change> m_Changes;
        std::vector<Cell> m_Cells;
        State m_CurrentState;
        uint8_t m_Dimention;
        uint8_t m_BlockDimention;
        std::vector<Conflict> m_Conflicts;
        std::vector<std::vector<CellFilter>> m_BlockFilter;
        bool autoPencil;
    };
}

#endif /* SUDOKUSTORAGE_H_ */
