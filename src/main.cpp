/*
 * main.cpp
 *
 *  Created on: 5 May 2019
 *      Author: Alf Henrik Sauge
 */

#include "gui/mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
