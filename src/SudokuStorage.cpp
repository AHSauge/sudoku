/*
 * Storage.cpp
 *
 *  Created on: 6 May 2019
 *      Author: Alf Henrik Sauge
 */

#include "SudokuStorage.h"

namespace Sudoku
{
    const uint8_t default_dim = 3;

    Storage::Storage()
     : autoPencil(false)
    {
        m_BlockDimention = default_dim;
        m_Dimention = m_BlockDimention * m_BlockDimention;
        uint16_t numCells = m_Dimention * m_Dimention;
        m_Cells.resize(numCells, Cell(false, 0, m_Dimention));
        m_Conflicts.resize(numCells, Conflict());
        m_CurrentState.Possibles.resize(numCells, std::vector<bool>(m_Dimention, true));
        m_CurrentState.PencilMarks.resize(numCells, std::vector<PencilMark>(m_Dimention, PencilMark::None));
        m_BlockFilter.resize(m_Dimention, std::vector<CellFilter>(m_Dimention));
        setDefaultBlockFilter();
    }

    Storage::~Storage()
    {
        // TODO Auto-generated destructor stub
    }

    uint16_t Storage::getNumCells() const
    {
        return m_Cells.size();
    }

    uint8_t Storage::getDimention() const
    {
        return m_Dimention;
    }

    uint8_t Storage::getBlockDimention() const
    {
        return m_BlockDimention;
    }

    bool Storage::setNumber(uint16_t cellID, uint8_t num)
    {
        auto& cell = m_Cells[cellID];
        if (cell.Locked == false)
        {
        	Change change;
        	change.PreviousState = m_CurrentState;
        	change.ChangedCell = &m_Cells[cellID];
        	change.PreviousValue = m_Cells[cellID].Number;
        	m_Changes.push(change);
            m_Cells[cellID].Number = num;
            updateConflict(m_Cells[cellID]);
            updatePossible();
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Storage::togglePencilMark(uint16_t cellID, uint8_t num)
    {
        auto idx = num - 1; 
        auto& cell = m_Cells[cellID];
        if (cell.Locked == false && m_CurrentState.Possibles[cellID][idx])
        {
            auto &mark = m_CurrentState.PencilMarks[cellID][idx];
            if (mark == PencilMark::None || mark == PencilMark::ManuallyRemoved)
            {
                mark = PencilMark::ManuallyFilled;
            }
            else
            {
                mark = PencilMark::ManuallyRemoved;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    uint8_t Storage::getNumber(uint16_t cellID)
    {
        return m_Cells[cellID].Number;
    }

    const Cell& Storage::getCell(uint16_t cellID) const
    {
        return m_Cells[cellID];
    }

    ConflictLevel Storage::getConflict(uint16_t cellID) const
    {
        return m_Conflicts[cellID].Level;
    }

    const std::vector<PencilMark>& Storage::getPencilMarks(uint16_t cellID) const
    {
        return m_CurrentState.PencilMarks[cellID];
    }

    void Storage::setDefaultBlockFilter()
    {
        for (uint8_t b = 0; b < m_Dimention; b++)
        {
            uint16_t blockStart = ((b % m_BlockDimention) * m_BlockDimention) + (b / m_BlockDimention) * m_Dimention*m_BlockDimention;
            for (uint8_t c = 0; c < m_Dimention; c++)
            {
                uint16_t cellID = blockStart + c % m_BlockDimention + ((c / m_BlockDimention) * m_Dimention);
                m_BlockFilter[b][c].ReferencedCell = &(m_Cells[cellID]);
                m_BlockFilter[b][c].ReferencedID = cellID;
                m_Cells[cellID].Block = b;
            }
        }
    }

    void Storage::undo()
    {
    	if (m_Changes.size() > 0)
    	{
    		const auto& last = m_Changes.top();
    		m_CurrentState = last.PreviousState;
    		last.ChangedCell->Number = last.PreviousValue;
    		m_Changes.pop();
    		updateConflict(*last.ChangedCell);
    	}
    }

    void Storage::setAutoPencil(bool autoOn)
    {
        autoPencil = autoOn;
        if (autoPencil)
        {
            updatePossible();
        }
    }

    void Storage::updatePossible()
    {
        std::vector<std::vector<bool>> rows(m_Dimention, std::vector<bool>(m_Dimention, true));
        std::vector<std::vector<bool>> columns(m_Dimention, std::vector<bool>(m_Dimention, true));
        std::vector<std::vector<bool>> blocks(m_Dimention, std::vector<bool>(m_Dimention, true));
        uint16_t cell = 0;
        for (uint8_t r = 0; r < m_Dimention; r++)
        {
            for (uint8_t c = 0; c < m_Dimention; c++, cell++)
            {
                if (m_Cells[cell].Number != 0)
                {
                    rows[r][m_Cells[cell].Number - 1] = false;
                    columns[c][m_Cells[cell].Number - 1] = false;
                    blocks[m_Cells[cell].Block][m_Cells[cell].Number - 1] = false;
                }
            }
        }
        cell = 0;
        for (uint8_t r = 0; r < m_Dimention; r++)
        {
            for (uint8_t c = 0; c < m_Dimention; c++, cell++)
            {
                for (uint8_t i = 0; i < m_Dimention; i++)
                {
                    auto &mark = m_CurrentState.PencilMarks[cell][i];
                	m_CurrentState.Possibles[cell][i] = rows[r][i] && columns[c][i] && blocks[m_Cells[cell].Block][i];
                    if (m_CurrentState.Possibles[cell][i])
                    {
                        //Automatic pencil fill
                        if (mark == PencilMark::None && autoPencil)
                        {
                            mark = PencilMark::AutoFilled;
                        }
                        //Automatic re-fill of manually entred marks
                        else if (mark == PencilMark::WasManuallyFilled)
                        {
                            mark = PencilMark::ManuallyFilled;
                        }
                    }
                    else if ( mark == PencilMark::AutoFilled)
                    {
                        mark = PencilMark::None;
                    }
                    else if (mark == PencilMark::ManuallyFilled)
                    {
                        mark = PencilMark::WasManuallyFilled;
                    }
                }
            }
        }
    }

    void Storage::updateConflict(Cell &source)
    {
        bool has_conflict = false;
        size_t numCells = m_Cells.size();
        m_Conflicts = std::vector<Conflict>(numCells, Conflict());
        for (size_t i = 0; i < numCells; i++)
        {
            auto& cell = m_Cells[i];
            if (cell.Number != 0)
            {
                //auto coord = getCoordinate(i);
                // uint8_t rowInBlock = coord.Row % m_BlockDimention;
                // uint8_t colInBlock = coord.Col % m_BlockDimention;
                // uint16_t tmp = (i - (i % m_BlockDimention)) % m_Dimention;

                //Check the rest of the row
                size_t rowStart = i % m_Dimention;
                size_t rowEnd = i - (rowStart) + m_Dimention;
                for (size_t r = i+1; r < rowEnd; r++)
                {
                    auto& other = m_Cells[r];
                    if (cell.Number == other.Number)
                    {
                        applyConflict(i, r, Placement::Row);
                        has_conflict = true;
                    }
                }

                //Check the rest of the column
                size_t colEnd = numCells - m_Dimention + rowStart + 1;
                for (size_t c = i+m_Dimention; c < colEnd; c += m_Dimention)
                {
                    auto& other = m_Cells[c];
                    if (cell.Number == other.Number)
                    {
                        applyConflict(i, c, Placement::Column);
                        has_conflict = true;
                    }
                }

                //Check the block
                uint8_t blockID = (((i / m_Dimention) / m_BlockDimention)*m_BlockDimention) + ((i % m_Dimention) / m_BlockDimention);
                for (uint8_t bc = 0; bc < m_Dimention; bc++)
                {
                    const CellFilter& cRef = m_BlockFilter[blockID][bc];
                    if (cRef.ReferencedCell != nullptr)
                    {
                        if (cRef.ReferencedID != i)
                        {
                            if (cRef.ReferencedCell->Number == cell.Number)
                            {
                                applyConflict(i, cRef.ReferencedID, Placement::Block);
                                has_conflict = true;
                            }
                        }
                    }
                }
            }
        }
        source.ConflictSource = has_conflict;
    }

    void Storage::applyConflict(uint16_t a, uint16_t b, Placement type)
    {
        m_Conflicts[a].Level = ConflictLevel::Direct;
        m_Conflicts[b].Level = ConflictLevel::Direct;
        auto coordA = getCoordinate(a);

        switch (type)
        {
        case Placement::Row:
            {
                uint16_t rowStart = (coordA.Row * m_Dimention);
                uint16_t rowEnd = rowStart + m_Dimention;
                for (uint16_t r = rowStart; r < rowEnd; r++)
                {
                    if (m_Conflicts[r].Level == ConflictLevel::None)
                    {
                        m_Conflicts[r].Level = ConflictLevel::SameDimention;
                    }
                }
            }
            break;

        case Placement::Column:
            {
                uint16_t colStart = coordA.Col;
                uint16_t colEnd = m_Cells.size() - m_Dimention + coordA.Col + 1;
                for (uint16_t c = colStart; c < colEnd; c += m_Dimention)
                {
                    if (m_Conflicts[c].Level == ConflictLevel::None)
                    {
                        m_Conflicts[c].Level = ConflictLevel::SameDimention;
                    }
                }
            }
            break;

        case Placement::Block:
            {
                //Check the block
                uint8_t blockID = (((a / m_Dimention) / m_BlockDimention)*m_BlockDimention) + ((a % m_Dimention) / m_BlockDimention);
                for (uint8_t bc = 0; bc < m_Dimention; bc++)
                {
                    const CellFilter& cRef = m_BlockFilter[blockID][bc];
                    if (cRef.ReferencedCell != nullptr)
                    {
                        if (m_Conflicts[cRef.ReferencedID].Level == ConflictLevel::None)
                        {
                            m_Conflicts[cRef.ReferencedID].Level = ConflictLevel::SameDimention;
                        }
                    }
                }
            }
            break;
        }
    }
}
