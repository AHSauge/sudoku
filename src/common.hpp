/*
 * common.hpp
 *
 *  Created on: 5 May 2019
 *      Author: Alf Henrik Sauge
 */

#ifndef SUDOKU_COMMON_HPP_
#define SUDOKU_COMMON_HPP_


enum class Direction
{
    Left, Right, Up, Down
};


#endif /* SUDOKU_COMMON_HPP_ */
