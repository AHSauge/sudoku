#ifndef CELLWIDGET_H
#define CELLWIDGET_H

#include "common.hpp"
#include "SudokuStorage.h"
#include <map>
#include <optional>
#include <QWidget>

class SudokuGrid;

class CellWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CellWidget(uint16_t cellID, Sudoku::Storage& storage, SudokuGrid *parent = nullptr);
    virtual ~CellWidget();
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;

private:
    QColor getCurrentBackground() const;

private:
    SudokuGrid* m_GridWidget;
    uint16_t m_CellID;
    std::map<Qt::Key, Direction> m_KeyDirMap;
    Sudoku::Storage& m_Storage;
};

#endif // CELLWIDGET_H
