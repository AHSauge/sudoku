#pragma once

#include "cellwidget.h"
#include "SudokuStorage.h"
#include <QFrame>
#include <QResizeEvent>
#include <QGridLayout>

class SudokuGrid : public QFrame
{
    Q_OBJECT
public:
    explicit SudokuGrid(QWidget *parent = nullptr);
    virtual ~SudokuGrid();
    virtual void resizeEvent(QResizeEvent *) override;
    void moveFocus(uint16_t cellID, Direction dir);
    void setCellNumber(uint16_t cellID, uint8_t number);
    void togglePencilMark(uint16_t cellID, uint8_t number);
    uint8_t getBlockDimention() const;
    virtual QSize sizeHint() const override;
    bool isPencilActive() const { return pencilActive; }
private:
    void setupGrid();

private:
    QGridLayout m_OuterGrid;
    std::vector<QGridLayout*> m_InnerGrid;
    std::vector<CellWidget*> m_Cells;
    Sudoku::Storage m_Storage;
    bool pencilActive;

signals:

public slots:
    void setAutoPencil(bool autoOn);
    void setPencilMode(bool pencilOn);
};
