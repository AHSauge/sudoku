
#include "cellwidget.h"
#include "sudokugrid.h"
#include <QBrush>
#include <QEvent>
#include <QFontDatabase>
#include <QPaintEvent>
#include <QPainter>

CellWidget::CellWidget(uint16_t cellID, Sudoku::Storage& storage, SudokuGrid *parent)
: QWidget(parent),
  m_GridWidget(parent),
  m_CellID(cellID),
  m_Storage(storage)
{
    setFocusPolicy(Qt::FocusPolicy::ClickFocus);
    m_KeyDirMap[Qt::Key_Left] = Direction::Left;
    m_KeyDirMap[Qt::Key_Right] = Direction::Right;
    m_KeyDirMap[Qt::Key_Up] = Direction::Up;
    m_KeyDirMap[Qt::Key_Down] = Direction::Down;
}

CellWidget::~CellWidget()
{

}

void CellWidget::paintEvent(QPaintEvent *event)
{
    QPainter paint;
    paint.begin(this);
    paint.setRenderHint(QPainter::Antialiasing);

    //Drawing cell background
    auto curBG = getCurrentBackground();
    paint.setBrush(QBrush(curBG));
    paint.drawRect(event->rect());

    QFont font = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    QString displayText;
    auto digit = m_Storage.getNumber(m_CellID);
    if (digit != 0)
    {
        font.setPixelSize(width() * 5 / 6);
        displayText = QString::number(digit);
    }
    else
    {
        const auto& marks = m_Storage.getPencilMarks(m_CellID);
        uint8_t dim = m_Storage.getBlockDimention();
        QChar nbsp = QChar(0xA0);
        QChar lf = QChar('\n');
        for (uint8_t r = 0, cell = 0; r < dim; r++)
        {
            for (uint8_t c = 0; c < dim; c++, cell++)
            {
                if (marks[cell] == Sudoku::PencilMark::AutoFilled || marks[cell] == Sudoku::PencilMark::ManuallyFilled)
                {
                    displayText += QString::number(cell + 1);
                }
                else
                {
                    displayText += nbsp;
                }
                if (c != 2)
                {
                    displayText += nbsp;
                }
            }
            if (r != 2)
            {
                displayText += lf;
            }
        }
        font.setPixelSize(width() / 4.25);
    }

    //TODO: Set colour
    paint.setFont(font);

    paint.drawText(event->rect(),
            Qt::AlignCenter,// | Qt::TextIncludeTrailingSpaces | Qt::TextJustificationForced,
            displayText);
}

void CellWidget::keyPressEvent(QKeyEvent *event)
{
    auto iter = m_KeyDirMap.find(static_cast<Qt::Key>(event->key()));
    if (iter != m_KeyDirMap.end())
    {
        m_GridWidget->moveFocus(m_CellID, iter->second);
    }
    else if (event->key() == Qt::Key_Delete)
    {
        m_GridWidget->setCellNumber(m_CellID, 0);
    }
    else if (event->key() >= Qt::Key_1 && event->key() <= Qt::Key_9)
    {
        if (m_GridWidget->isPencilActive())
        {
            m_GridWidget->togglePencilMark(m_CellID, event->key() - Qt::Key_0);
        }
        else
        {
            m_GridWidget->setCellNumber(m_CellID, event->key() - Qt::Key_0);
        }
    }
    else if (event->key() >= Qt::Key_F1 && event->key() <= Qt::Key_F9)
    {
        m_GridWidget->togglePencilMark(m_CellID, event->key() - Qt::Key_F1 + 1);
    }
    else if (event->key() == Qt::Key_Z && event->modifiers() == Qt::ControlModifier)
    {
    	m_Storage.undo();
    	m_GridWidget->update();
    }
}

QColor CellWidget::getCurrentBackground() const
{
    auto conflict = m_Storage.getConflict(m_CellID);
    QWidget tmpWidget;
    QColor c = tmpWidget.palette().color(QPalette::Window);
    if (conflict == Sudoku::ConflictLevel::Direct)
    {
        c = tmpWidget.palette().color(QPalette::Highlight);
        c.setAlpha(150);
    }
    else if (conflict == Sudoku::ConflictLevel::SameDimention)
    {
        c = tmpWidget.palette().color(QPalette::Highlight);
        c.setAlpha(200);
    }

    if (hasFocus())
    {
        c = tmpWidget.palette().color(QPalette::Highlight);
    }
    return c;
}
