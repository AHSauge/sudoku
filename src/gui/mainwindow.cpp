#include <qtoolbutton.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->hintAccept->hide();
    ui->hintButton->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}
