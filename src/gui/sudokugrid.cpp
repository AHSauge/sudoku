#include "sudokugrid.h"

SudokuGrid::SudokuGrid(QWidget *parent)
: QFrame(parent),
  m_OuterGrid(this),
  pencilActive(false)
{
    setupGrid();

    QPalette p = palette();
    p.setColor(QPalette::Window, Qt::black);
    setAutoFillBackground(true);
    setPalette(p);
    sizePolicy().setWidthForHeight(true);
    sizeHint();
}

SudokuGrid::~SudokuGrid()
{

}

void SudokuGrid::resizeEvent(QResizeEvent *event)
{
    event->accept();
    //Ensure quadratic size
    auto minSize = std::min(event->size().width(), event->size().height());
    resize(minSize, minSize);
}
QSize SudokuGrid::sizeHint() const
{
    return QSize(width(), width());
}

void SudokuGrid::moveFocus(uint16_t cellID, Direction dir)
{
    uint16_t width = m_Storage.getDimention();
    uint16_t numCells = m_Storage.getNumCells();
    if (dir == Direction::Down)
    {
        if (cellID < (numCells - width))
        {
            cellID += width;
        }
    }
    else if (dir == Direction::Up)
    {
        if (cellID >= width)
        {
            cellID -= width;
        }
    }
    else
    {
        uint16_t col = cellID % width;
        if (dir == Direction::Left)
        {
            if (col > 0)
            {
                cellID--;
            }
        }
        else if (dir == Direction::Right)
        {
            if (col < (width - 1))
            {
                cellID++;
            }
        }
    }
    m_Cells[cellID]->setFocus();
}

void SudokuGrid::setCellNumber(uint16_t cellID, uint8_t number)
{
    if (m_Storage.setNumber(cellID, number))
    {
        update();
    }
}

void SudokuGrid::togglePencilMark(uint16_t cellID, uint8_t number)
{
    if (m_Storage.togglePencilMark(cellID, number))
    {
        update();
    }
}

uint8_t SudokuGrid::getBlockDimention() const
{
    return m_Storage.getBlockDimention();
}

void SudokuGrid::setPencilMode(bool pencilOn)
{
    pencilActive = pencilOn;
}

void SudokuGrid::setAutoPencil(bool autoOn)
{
    m_Storage.setAutoPencil(autoOn);
    if (autoOn)
    {
        update();
    }
}

void SudokuGrid::setupGrid()
{
    m_OuterGrid.setContentsMargins(0, 0, 0, 0);
    m_OuterGrid.setHorizontalSpacing(4);
    m_OuterGrid.setVerticalSpacing(4);

    uint16_t numBlocks = m_Storage.getDimention();
    uint8_t blockDim = m_Storage.getBlockDimention();
    m_InnerGrid.reserve(numBlocks);
    for (uint16_t i = 0; i < blockDim; i++)
    {
        for (uint16_t j = 0; j < blockDim; j++)
        {
            QGridLayout* g = new QGridLayout();
            g->setContentsMargins(0, 0, 0, 0);
            g->setHorizontalSpacing(2);
            g->setVerticalSpacing(2);
            m_InnerGrid.push_back(g);
            m_OuterGrid.addLayout(m_InnerGrid.back(), i, j);
        }
    }

    uint16_t numCells = m_Storage.getNumCells();
    m_Cells.resize(numCells);
    for (uint16_t cell = 0; cell < numCells; cell++)
    {
        uint16_t row = cell / numBlocks;
        uint16_t col = cell % numBlocks;
        uint16_t innerRow = row % blockDim;
        uint16_t innerCol = col % blockDim;
        uint16_t subBlockRow = row / blockDim;
        uint16_t subBlockCol = col / blockDim;
        uint16_t subBoxNum = (subBlockRow * blockDim) + subBlockCol;

        CellWidget* c = new CellWidget(cell, m_Storage, this);
        m_Cells[cell] = c;
        m_InnerGrid[subBoxNum]->addWidget(c, innerRow, innerCol);
    }

}
